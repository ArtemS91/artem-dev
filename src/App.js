import { About } from "./components/About";
import { Contacts } from "./components/Contacts";
import { Header } from "./components/Header";
import { Projects } from "./components/Projects";
import { Skills } from "./components/Skills";


function App() {
  return (
    <>
          <Header/>
          <About/>
          <Skills/>
          <Projects/>
          <Contacts/>
    </>  
  );
}

export default App;
